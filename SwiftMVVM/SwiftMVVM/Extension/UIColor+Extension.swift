//
//  UITableView+Extension.swift
//  View
//
//  Created by Giáp Trần on 11/13/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(hex: Int, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    public static let primary = UIColor(hex: 0xDD4741)
    
    public static let refresh = [UIColor(hex: 0xDD4741),
                                 UIColor(hex: 0xFF5722),
                                 UIColor(hex: 0x4b4700),
                                 UIColor(hex: 0x27ae60),
                                 UIColor(hex: 0x2196F3)]

}
