//
//  UIView+View.swift
//  View
//
//  Created by Giáp Trần on 12/22/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit

extension UIView {

    public func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(
            roundedRect         : bounds,
            byRoundingCorners   : corners,
            cornerRadii         : CGSize(width: radius, height: radius)
        )
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}
