//
//  UITableView+Extension.swift
//  View
//
//  Created by Giáp Trần on 11/13/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit
import SpringIndicator

extension UITableView {
    
    public var springIndicator: SpringIndicator {
        let springIndicator = SpringIndicator(frame: CGRect(x: 0, y: 0, width: 20.0, height: 20.0))
        springIndicator.setupDefault()
        return springIndicator
    }
    
    public func clearSeparator() {
        tableFooterView = UIView()
    }
    
    public func updateIndicatorFrame() {
        if let headerView = tableHeaderView,
            let springIndicator = headerView.subviews.first as? SpringIndicator {
            
            headerView.frame = CGRect(x: 0, y: 0, width: frame.width, height: 60.0)
            springIndicator.center = headerView.center
        }
        if let footerView = tableFooterView,
           let springIndicator = footerView.subviews.first as? SpringIndicator {
          
            footerView.frame = CGRect(x: 0, y: 0, width: frame.width, height: 60.0)
            springIndicator.center = footerView.center
        }
    }
    
    public func startHeaderLoading() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 60.0))
        let springIndicator = self.springIndicator
        springIndicator.startAnimation()
        springIndicator.center = headerView.center
        headerView.addSubview(springIndicator)
        tableHeaderView = headerView
    }
    
    public func stopHeaderLoading(isAwaiting: Bool = true) {
        if let springIndicator = tableHeaderView?.subviews.first as? SpringIndicator {
            springIndicator.stopAnimation(isAwaiting) { [weak self] _ in
                guard let sSelf = self else { return }
                
                sSelf.tableHeaderView = nil
            }
        }
    }
    
    public func startFooterLoading() {
        tableFooterView?.isHidden = false
        if let springIndicator = tableFooterView?.subviews.first as? SpringIndicator {
            springIndicator.startAnimation()
        } else {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 60.0))
            let springIndicator = self.springIndicator
            springIndicator.center = footerView.center
            springIndicator.startAnimation()
            footerView.addSubview(springIndicator)
            
            tableFooterView = footerView
        }
    }

    public func stopFooterLoading(isAwaiting: Bool = true) {
        if let springIndicator = tableFooterView?.subviews.first as? SpringIndicator {
            springIndicator.stopAnimation(isAwaiting) {  [weak self] _ in
                guard let sSelf = self else { return }
                
                sSelf.tableFooterView?.isHidden = true
            }
        }
    }
    
    public func validIndexPath(_ indexPath: IndexPath) -> Bool {
        return indexPath.section >= 0 &&
               indexPath.section < numberOfSections &&
               indexPath.row < numberOfRows(inSection: indexPath.section)
    }
    
    public func scrollToTop(animated: Bool) {
        scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: animated)
    }
    
    public func scrollToBottom(animated: Bool) {
        let lastSection = numberOfSections - 1
        if lastSection < 0 {
            return
        }
        let lastRow = numberOfRows(inSection: lastSection) - 1
        let indexPath = IndexPath(row: lastRow, section: lastSection)
        scrollToRow(at: indexPath, at: .bottom, animated: animated)
    }
    
}

extension UITableViewRowAction {
    
    public static func rowAction(
        style aStyle    : UITableViewRowActionStyle,
        backgroundColor : UIColor,
        image           : UIImage,
        height          : CGFloat,
        handler         : @escaping (UITableViewRowAction, IndexPath) -> Void) -> UITableViewRowAction {
        
        let frame = CGRect(x: 0, y: 0, width: image.size.width + 40, height: height)
        
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: frame.size.width, height: frame.size.height),
            true,
            UIScreen.main.scale
        )
        let context = UIGraphicsGetCurrentContext()!
        
        backgroundColor.set()
        context.fill(frame)
        
        let point = CGPoint(x: frame.midX - (image.size.width / 2.0), y: frame.midY - (image.size.height / 2.0))
        
        image.draw(at: point)
        
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext();
        
        let emptySpaceWidth = " ".width(limited: height, font: UIFont.systemFont(ofSize: 13.0))
        let numberSpace = ceil(image.size.width / emptySpaceWidth)
        let title = "".padding(toLength: Int(numberSpace), withPad: " ", startingAt: 0)
        
        let action = UITableViewRowAction(style: aStyle, title: title, handler: handler)
        action.backgroundColor = UIColor(patternImage: backgroundImage)
        
        return action
    }
    
}
