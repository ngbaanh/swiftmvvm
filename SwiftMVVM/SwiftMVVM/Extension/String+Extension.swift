//
//  String+Extension.swift
//  View
//
//  Created by Giáp Trần on 11/14/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit

extension String {
    
    public func width(limited height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil
        )
        
        return boundingBox.width
    }
    
    public func height(limited width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(
            with        : constraintRect,
            options     : .usesLineFragmentOrigin,
            attributes  : [NSFontAttributeName: font],
            context     : nil
        )
        
        return boundingBox.height
    }
    
    public static var random: String {
        return UUID().uuidString
    }
    
}
