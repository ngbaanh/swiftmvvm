//
//  SpringIndicator+View.swift
//  View
//
//  Created by Giáp Trần on 12/28/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit
import SpringIndicator

extension SpringIndicator {
    
    public func setupDefault() {
        var colorIndex = 0
        
        lineColor = UIColor.refresh[colorIndex]
        lineWidth = 2
        rotateDuration = 1
        strokeDuration = 0.5
        intervalAnimationsHandler = { [weak self] (indicator) in
            guard let _ = self else { return }
            
            colorIndex += 1
            if colorIndex >= UIColor.refresh.count {
                colorIndex = 0
            }
            indicator.lineColor = UIColor.refresh[colorIndex]
        }
    }
    
}
