//
//  KeychainSwift+Model.swift
//  View
//
//  Created by Giáp Trần on 12/7/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import KeychainSwift
import JWT

extension KeychainSwift {

    private static let JwtKey = "JWT"
    
    public static let JwtSecret = "Jow3cFK4EKQzsC/M7VOFcKOcNYuKjzBsf26Tap5Earg="
    
    public static var jwt: String? {
        get {
            let keychain = KeychainSwift()
            return keychain.get(KeychainSwift.JwtKey)
        }
        set {
            let keychain = KeychainSwift()
            if let hasValue = newValue {
                keychain.set(hasValue, forKey: KeychainSwift.JwtKey)
            } else {
                keychain.delete(KeychainSwift.JwtKey)
            }
        }
    }
    
//    public static var isJWTExpired: Bool {
//        if let token = jwt {
//            let payload = try? JWT.decode(token, algorithm: .hs256(JwtSecret.data(using: .utf8)!))
//            if let expiredAt = payload?["exp"] as? TimeInterval {
//                let today = Date().timeIntervalSince1970
//                return expiredAt - today > 0
//            }
//        }
//        return false
//    }
    
}
