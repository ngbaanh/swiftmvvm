//
//  UICollectionView+Extension.swift
//  View
//
//  Created by Giáp Trần on 11/15/16.
//  Copyright © 2016 Giáp Trần. All rights reserved.
//

import UIKit
import SpringIndicator

extension UICollectionView {
    
    internal func setupIndicatorFooter() {
        register(
            UICollectionReusableView.self,
            forSupplementaryViewOfKind: UICollectionElementKindSectionFooter,
            withReuseIdentifier: UICollectionElementKindSectionFooter
        )
    }
    
    internal func indcatorFooter(frame: CGRect, color: UIColor, indexPath: IndexPath) -> UICollectionReusableView {
        let footerView = dequeueReusableSupplementaryView(
            ofKind: UICollectionElementKindSectionFooter,
            withReuseIdentifier: UICollectionElementKindSectionFooter,
            for: indexPath
        )
        
        if let _ = footerView.subviews.first as? SpringIndicator {
            return footerView
        }
        
        let springIndicator = SpringIndicator(frame: CGRect(x: frame.midX - 15, y: frame.midY - 15, width: 30.0, height: 30.0))
        springIndicator.lineWidth = 2
        springIndicator.rotateDuration = 1
        springIndicator.strokeDuration = 0.5
        springIndicator.startAnimation()
        
        footerView.addSubview(springIndicator)
        
        return footerView
    }

}
