# SwiftMVVM #

Lightweight Framework + Tools for MVVM Architecture.

### Cocoapods ###

```sh
pod 'SwiftMVVM', :git => 'https://bitbucket.org/ngbaanh/swiftmvvm.git'
```

### Thanks ###

* Inspired by: Giap Tran (https://bitbucket.org/itfgiap/)

### Dependencies ###

* Alamofire
* SwiftyJSON
* KeychainSwift
* EventBusSwift
* RxSwift
* RxCocoa
* SpringIndicator
* JSONWebToken